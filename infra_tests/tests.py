from google.cloud import container
from kubernetes import client, config
import os
import json
import re
# Set variables for expected and recieved results 
namespaces_expected = {
    "development",
    "default",
    "kube-public",
    "kube-system",
    "production",
    "monitoring",
    "staging"
}
namespaces_from_kuber = set()
# Set variables 
WORK_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) # Path to workdir
TEERAFORM_DIR =  WORK_DIR + "/terraform_kubernetes/" # Path to terraform files
KUBERNETES_DIR = WORK_DIR + "/kubernetes_resources/" # Path to dir with kubernetes files
TEST_DIR = os.path.dirname(os.path.abspath(__file__)) # Path to dir with test
GCP_KEY = TEERAFORM_DIR + "keys/key.json" # Path to gcp key
SECRET_VARIABLES = TEERAFORM_DIR + "terraform.tfvars"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.abspath(GCP_KEY) # Authentication
#
def cluster_cheking():
    expected_cluster_name = {"kubernetes-cluster"}
    recieved_cluster_names = set()
    secret_vars = {}
    with open(SECRET_VARIABLES,"r") as file:
        for line in file.read().splitlines():
            name,value = line.split("=")
            secret_vars[name.strip()] = value.strip().strip('\"')
    client = container.ClusterManagerClient()
    clusters = client.list_clusters(project_id = secret_vars['PROJECT'],zone = secret_vars['ZONE'])
    for cluster in clusters.clusters:
        recieved_cluster_names.add(cluster.name)
    return bool(recieved_cluster_names & expected_cluster_name)
def namespace_checking():
# Cheking for namespaces
    config.load_kube_config() # don't set config_file param as default param is suitable
    v1 = client.CoreV1Api()
    response = v1.list_namespace()
    for item in response.items:
        namespaces_from_kuber.add(item.metadata.name)
    return namespaces_from_kuber==namespaces_expected
#
def monitring_checking():
    pods_expected = {"prometheus","grafana"}
    pods_from_kuber = set()
    config.load_kube_config()
    response = client.CoreV1Api()
    monitoring_pods = response.list_namespaced_pod("monitoring")  
    for item in monitoring_pods.items:
        if item.status.phase == 'Running':
            pods_from_kuber.add(item.metadata.name.split("-")[0])
    return pods_from_kuber == pods_expected


def sample_app_checking():
    pod_expected = "sample-app"
    config.load_kube_config()
    response = client.CoreV1Api()
    monitoring_pods = response.list_namespaced_pod("development")  
    for item in monitoring_pods.items:
        if item.status.phase == 'Running':
            pod_from_kuber = item.metadata.name
    return bool(re.search(pod_expected,pod_from_kuber))
    
