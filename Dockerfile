FROM alpine:3.11
ARG USER=appuser
RUN apk update && \
apk add --no-cache  git-crypt wget curl git && \
wget -q https://github.com/sapcc/kubernikus/releases/download/v1.0.0%2B7bb18d9b1511e9da1bfe5cb2ed332699ca4f8f7f/kubernikusctl_linux_amd64 && \
mv kubernikusctl_linux_amd64 kubernikusctl && \
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.0/bin/linux/amd64/kubectl && \
chmod 755 kubectl && \
chmod 755 kubernikusctl && \
mv kubectl /usr/local/bin  && \
mv kubernikusctl /usr/local/bin && \
apk del sudo  && \
rm -rf /var/cache/apk && \
rm -rf /var/lib/apk && \
addgroup -S appgroup && adduser -S ${USER} -G appgroup
USER ${USER}
WORKDIR /home/${USER}
ENTRYPOINT ["sh"]



