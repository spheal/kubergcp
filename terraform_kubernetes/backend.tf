provider "google" {
  credentials = "${file("keys/key.json")}"
  project = "${var.PROJECT}"
  region = "${var.REGION}"
}
# GCP doesn't support feature that create bucket while initializing backend
# gcloud auth activate-service-account --key-file keys/key.json
# gsutil mb -l us-east1 -p <project> -c regional gs://infra-deploy
terraform {
    backend "gcs" {
        bucket = "infra-deploy"
        prefix  = "terraform/state"
        credentials = "keys/key.json"
    }
}
