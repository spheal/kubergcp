resource "google_container_cluster" "kubernetes_cluster" {
    name = "kubernetes-cluster"
    location = "${var.ZONE}"
    remove_default_node_pool = true
    initial_node_count = 1
    monitoring_service = "none"
}
resource "google_container_node_pool" "kubernetes_node_cluster" {
    name = "kubernetes-cluster"
    location = "${var.ZONE}"
    cluster = "${google_container_cluster.kubernetes_cluster.name}"
    node_count = 1
    node_config {
        oauth_scopes = [ # API thath accessable by all the nodes under default service account in kubernetes
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring",
            "https://www.googleapis.com/auth/devstorage.read_only", # Pull images from GCR
        ]
        preemptible  = true 
        machine_type = "n1-standard-4"
        disk_size_gb = 15
        labels = {
            service = "kuberntes",
            type = "preemtible"
        }
        disk_type = "pd-standard"
        image_type = "COS_CONTAINERD"
    }
    autoscaling {
        min_node_count = 0
        max_node_count = 3
    }
}

resource "null_resource" "get_kubectl_config" {
    provisioner "local-exec" {
        command = "gcloud container clusters get-credentials ${google_container_node_pool.kubernetes_node_cluster.name} --zone=${var.ZONE}"
    }
}




# resource "google_container_node_pool" "kubernetes_node_cluster2" {
#     name = "kubernetes-cluster2"
#     location = "${var.ZONE}"
#     cluster = "${google_container_cluster.kubernetes_cluster.name}"
#     node_count = 1
#     node_config {
#         oauth_scopes = [ # API thath accessable by all the nodes under default service account in kubernetes
#             "https://www.googleapis.com/auth/logging.write",
#             "https://www.googleapis.com/auth/monitoring",
#             "https://www.googleapis.com/auth/devstorage.read_only", # Pull images from GCR
#         ]
#         machine_type = "g1-small"
#         disk_size_gb = 15
#         labels = {
#             service = "kuberntes",
#             type = "non-preemtible"
#         }
#         disk_type = "pd-standard"
#         image_type = "COS_CONTAINERD"
#     }
#     autoscaling {
#         min_node_count = 0
#         max_node_count = 3
#     }
# }
