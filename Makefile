WORKDIR=$(shell pwd)
KUBER_WORKDIR="$(WORKDIR)/kubernetes_resources"
TEST_WORKDIR="$(WORKDIR)/infra_tests"
TERRAFORM_WORKDIR="$(WORKDIR)/terraform_kubernetes"

simple_cluster_deploy: cluster_deploy namespace_deploy cluster_test
app_cluster_deploy: cluster_deploy namespace_deploy monitoring_deploy app_deploy cluster_test app_test 

cluster_deploy:
	@cd $(TERRAFORM_WORKDIR); terraform apply --auto-approve -lock=false

namespace_deploy: 
	@kubectl apply -f $(KUBER_WORKDIR)/namespaces/

monitoring_deploy: 
	@kubectl apply -f $(KUBER_WORKDIR)/monitoring_stack/

app_deploy: 
	@kubectl apply -f $(KUBER_WORKDIR)/deployments_sample_app/

cluster_destroy:
	@cd $(TERRAFORM_WORKDIR); terraform destroy --auto-approve -lock=false

dependencies_install:
	@pip3 install -r $(TEST_WORKDIR)/requirements.txt
	@echo "-----------------------------------------------------------------"

cluster_test: dependencies_install
	@python3 $(TEST_WORKDIR)/cluster_namespace.py

app_test: 
	@python3 $(TEST_WORKDIR)/monitoring_sampleapp.py

